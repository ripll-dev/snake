#ifndef SNAKE_H
#define SNAKE_H


#include <QRect>
#include "gamewidget.h"
#include "gameobject.h"


class Snake : public GameObject
{

    QVector<QRect> blocks;
    int N{29}, M{19}, block_size{30};
    int route{0};
    int l_route{0};
    int score;
    bool game_over{false};
    bool checkGameOver(QRect);

    /* route:
         0 - вправо
         1 - вверх
         2 - влево
         3 - вниз
    */


public:
    Snake();
    bool apple_earned = false;
    QVector<QRect> getSnake();
    void paintObj(GameWidget*);
    void setRoute(int r);

public slots:
    void tick();
};

#endif // SNAKE_H
