#include "snake.h"
#include "state.h"
#include "QDebug"


Snake::Snake() {

    route = 0;
    score = 0;
    blocks.append(QRect(3+30*5,3+30*5,24,24));
}
void Snake::setRoute(int r) {
    if ((l_route == 0 && r == 2) ||
         l_route == 1 && r == 3  ||
         l_route == 2 && r == 0  ||
         l_route == 3 && r == 1) {

    } else {
        route = r;
    }

}

void Snake::tick() {
    QRect last = blocks[0];

    if (route == 0) {
        last.setRect(blocks[0].x()+30, blocks[0].y(),24,24);
    } else if (route == 1) {
        last.setRect(blocks[0].x(), blocks[0].y()-30,24,24);
    } else if (route == 2) {
        last.setRect(blocks[0].x()-30, blocks[0].y(),24,24);
    } else if (route == 3) {
        last.setRect(blocks[0].x(), blocks[0].y()+30,24,24);
    }

    if (!apple_earned)
        blocks.pop_back();
    game_over = checkGameOver(last);
    blocks.insert(0,last);
    l_route = route;
    apple_earned = false;

}

void Snake::paintObj(GameWidget* wind) {
    QPainter painter(wind);
    QPen pen{QColor("#ffffff")};
    painter.setBrush(QBrush(QColor("#ffffff")));
    painter.setPen(pen);
    painter.drawRects(blocks);
    if (game_over) {
        wind->game_over = true;
    }

}


QVector<QRect> Snake::getSnake() {
    return blocks;
}

bool Snake::checkGameOver(QRect rect) {
    int x = rect.x();
    int y = rect.y();
    //check walls
    if (x > 870 || x < 30 || y < 60 || y > 570 ||
    //check snake
            blocks.count(QRect(x,y,24,24))>0) {

        return true;
    } else return false;
}
