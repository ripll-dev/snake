#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include "QDebug"
#include <QWidget>
#include <QPainter>
#include <QPalette>
#include <QColor>
#include <QTimer>
#include <QImage>
#include <QPixmap>


class GameWidget : public QWidget
{
    Q_OBJECT


public:
    int game_score{0};
    QString nickname{""};
    class State *current;
    bool game_over{false};
    int score{0};
    GameWidget(QWidget *parent = 0);
    ~GameWidget();


    void setCurrent(class State* m) {
        //delete current;
        current = m;
    }

    virtual void paintEvent(QPaintEvent*);
    virtual void mouseReleaseEvent(QMouseEvent*);
    virtual void keyPressEvent(QKeyEvent*);
    void toGameOver(int);


public slots:
    void nextAnimationFrame();

private:

    QPainter* painter;

};

#endif // GAMEWIDGET_H
