#ifndef WALL_H
#define WALL_H
#include "gameobject.h"

class Wall : public GameObject
{
    QVector<QRect> walls;
public:
    Wall();
    void paintObj(GameWidget*wind);

};

#endif // WALL_H
