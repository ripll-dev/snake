#ifndef APPLE_H
#define APPLE_H
#include "gameobject.h"

class Apple : public GameObject
{
    QVector<QRectF> apples;
public:
    void paintObj(GameWidget*wind);
    Apple();

};

#endif // APPLE_H
