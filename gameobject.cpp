#include "gameobject.h"
#include "snake.h"
#include "apple.h"
#include "wall.h"


GameObject *GameObject::createObject(OBJECT_ID id)
{
    GameObject *object;
    switch ( id ) {
    case WALL:
        object = new Wall();
        break;
    case SNAKE:
        object = new Snake();
        break;
    case APPLE:
        object = new Apple();
        break;
    }
    return object;
}
