#include "factory.h"

Factory::Factory()
{

}


#include <iostream>
#include <vector>

// Иерархия классов игровых персонажей
class GameObj
{
  public:
    virtual void info() = 0;
    virtual ~Warrior() {}
};


// Фабрики объектов
class Factory
{
  public:
    virtual Warrior* createWarrior() = 0;
    virtual ~Factory() {}
};

class InfantryFactory: public Factory
{
  public:
    Warrior* createWarrior() {
      return new Infantryman;
    }
};

class ArchersFactory: public Factory
{
  public:
    Warrior* createWarrior() {
      return new Archer;
    }
};

class CavalryFactory: public Factory
{
  public:
    Warrior* createWarrior() {
      return new Horseman;
    }
};
