#include "game.h"

Game::Game(){
    this->window = new GameWidget;
}

void Game::run(){
    this->window->setFixedSize(900, 600);
    this->window->setWindowTitle("Snake || by Ripll");
    this->window->show();
}



Game::~Game(){
    delete this->window;
}
