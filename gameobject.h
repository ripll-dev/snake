#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "gamewidget.h"
#include "random"
#include "time.h"

enum OBJECT_ID{ WALL = 0, SNAKE, APPLE };

class GameObject

{

    public:
        virtual void paintObj(GameWidget*) = 0;
        static GameObject *createObject( OBJECT_ID);
};

#endif // GAMEOBJECT_H
