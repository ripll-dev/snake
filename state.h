#ifndef STATE_H
#define STATE_H

#include <gamewidget.h>
#include <game.h>
#include <QMouseEvent>
#include <QFontDatabase>
#include "gameobject.h"
#include <QtAlgorithms>

class State : public QObject
{
public:
    State();
    //Инициализация картинок
    QImage start_btn{":/images/start_menu_btn.png"};
    QImage exit_btn{":/images/exit.png"};
    QImage rating_btn{":/images/rating.png"};
    QImage menu_btn{":/images/menu.png"};
    QImage enter_btn{":/images/enter.png"};
    QPen pen{QColor("#ffffff")};
    int score{0};


    //Загрузка шрифта
    int id = QFontDatabase::addApplicationFont(":/fonts/font1.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont font_big{family, 100};
    QFont font_normal{family, 50};
    QFont font_small{family, 20};
    int frameNumber{1};


    virtual void paint(GameWidget*) {

    }
    virtual void mouseClick(QMouseEvent*, GameWidget*) {

    }
    virtual void keyPressEvent(QKeyEvent*, GameWidget*) {

    }


    float flyingBtn(int start_x, int end_x, int time, int cur_t, int mode);

};


class MainMenu : public State {

public:
    MainMenu() {
        frameNumber = 0;
    }

    virtual void paint(GameWidget*);
    virtual void mouseClick(QMouseEvent*, GameWidget*);
    virtual void keyPressEvent(QKeyEvent*, GameWidget*);
};

class LoadMenu : public State {

public:


    virtual void paint(GameWidget*);
};

class RatingAdd : public State {

public:
    RatingAdd();
    void add_score(GameWidget *m);
    virtual void paint(GameWidget*);
    virtual void mouseClick(QMouseEvent*, GameWidget*);
    virtual void keyPressEvent(QKeyEvent*, GameWidget*);
};

class Rating : public State {
    QMultiMap<unsigned, QString> RatingData;
public:
    Rating();
    virtual void paint(GameWidget*);
    virtual void mouseClick(QMouseEvent*, GameWidget*);
    virtual void keyPressEvent(QKeyEvent*, GameWidget*);

};

class GameWindow : public State {
    Q_OBJECT

    GameObject* snake;
    GameObject* apple;
    GameObject* wall;

public:
    GameWindow();
    QTimer* timer4tick;
    virtual void paint(GameWidget*);
    virtual void mouseClick(QMouseEvent*, GameWidget*);
    virtual void keyPressEvent(QKeyEvent*, GameWidget*);
    void apple_earned();
    QVector<QRect> getSnake();

public slots:
    void tick();
};

class GameOver : public State {
public:
    GameOver() {
        frameNumber = 0;
    }

    virtual void paint(GameWidget*);
    virtual void mouseClick(QMouseEvent*, GameWidget*);
    virtual void keyPressEvent(QKeyEvent*, GameWidget*);
};


#endif // STATE_H
