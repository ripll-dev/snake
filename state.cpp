#include "state.h"
#include "gameobject.h"
#include <math.h>
#include "snake.h"
#include <fstream>
#include <QDebug>
using namespace std;

State::State() {

}

//                     старт. т.  ||  конеч. т.|| время на аним. ||  текущий кадр || режим. 1 - от быстр., 0 - от медленного
float State::flyingBtn(int start_x, int end_x, int time, int cur_t, int mode) {
    if (cur_t >= time) {
        return end_x;
    } else {
        //Формула возвращает положение кнопочки в текущий момент времени;
        float x = start_x + ((2.*(end_x-start_x)*cur_t)/time) - ((1.*(end_x-start_x)*cur_t*cur_t)/(time*time));
        float x_1 = start_x + ((1.*(end_x-start_x)*cur_t*cur_t)/(time*time));
        return mode==1?x:x_1;
    }
}

//Загрузочный экран



void LoadMenu::paint(GameWidget *wind) {
    QPainter painter(wind);
    float t = frameNumber;

    painter.drawImage(304,flyingBtn(600, 240, 1000, t,1), start_btn, 0, 0, 0, 0);


    if (frameNumber == 1000){
        wind->setCurrent(new MainMenu());
    }
}


// Меню
void MainMenu::paint(GameWidget *wind) {
    QPainter painter(wind);
    painter.drawImage(304, 240, start_btn, 0, 0, 0, 0);
    painter.drawImage(408, 380, rating_btn, 0, 0, 0, 0);
    painter.drawImage(35, 35, exit_btn, 0, 0, 0, 0);

}

void MainMenu::mouseClick(QMouseEvent* event, GameWidget* m) {
    int x = event->pos().x();
    int y = event->pos().y();

    if(start_btn.valid( x-304 , y-240 )) {
        m->nickname = "";
        m->setCurrent(new GameWindow());
    }
    if(rating_btn.valid( x-408 , y-380 )) {
        m->setCurrent(new Rating());

    }
    if(exit_btn.valid( x-35 , y-35 )) {
        exit(0);
    }
    event->accept();
}

void MainMenu::keyPressEvent(QKeyEvent* event, GameWidget* m) {

    if (event->key()==Qt::Key_Escape) {
        exit(0);
    }
    if (event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return) {
        m->nickname = "";
        m->setCurrent(new GameWindow());
    }
    event->accept();


}
//____________________
Rating::Rating() {
    ifstream file;
    file.open("rating.dat", ios::in);
    int points;
    char name[30];

    while (file >> name >> points  ) {
          RatingData.insert( points, QString( name ) );
    }
    file.close();
}

void Rating::paint(GameWidget *wind) {
    QPainter painter(wind);
    painter.setFont(font_normal);
    painter.setPen(pen);
    painter.drawImage(20, 20, menu_btn, 0, 0, 0, 0);
    painter.drawText(QPoint(340,100), "Scores:");

    auto num = RatingData.keyEnd();
    auto it = RatingData.cend();
    QFontMetrics fm(font_normal);

    for (int i = 0 ; it != RatingData.cbegin() && i<5; i++) {
        --it;
        --num;
        painter.drawText(QPoint(75, 200+85*i),*it);
        painter.drawText(QPoint(815-fm.width(QString::number(*num)), 200+85*i), QString::number(*num));

    }

}

void Rating::mouseClick(QMouseEvent* event, GameWidget* m) {
    int x = event->pos().x();
    int y = event->pos().y();

    if(menu_btn.valid( x-20 , y-20 )) {
        m->setCurrent(new MainMenu());
    }

    event->accept();
}
void Rating::keyPressEvent(QKeyEvent* event, GameWidget* m) {

    if (event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return) {
        //add to rating
        m->setCurrent(new MainMenu());
    }
    event->accept();


}

//______________________________

GameWindow::GameWindow() {
    snake = GameObject::createObject(SNAKE);
    apple = GameObject::createObject(APPLE);
    wall = GameObject::createObject(WALL);
    timer4tick = new QTimer(this);
    connect (timer4tick, SIGNAL(timeout()), this, SLOT(tick()));

}

void GameWindow::tick() {
    timer4tick->start(300-score*5);
    dynamic_cast<Snake*>(snake)->tick();
}

void GameWindow::apple_earned() {
    score += 1;
    dynamic_cast<Snake*>(snake)->apple_earned = true;
}


QVector<QRect> GameWindow::getSnake() {
    return dynamic_cast<Snake*>(snake)->getSnake();
}

void GameWindow::paint(GameWidget *wind) {

    QPainter painter(wind);
    painter.setFont(font_big);
    painter.setPen(pen);

    int t = frameNumber;
    painter.drawImage(20, 10, menu_btn, 0, 0, 0, 0);
    //Обратный отсчет
    if (t < 500) {
        painter.drawText(QPoint(flyingBtn(900, 450, 500, t,1),250), "3");
    } else if (t >= 500 && t < 1000) {
        painter.drawText(QPoint(flyingBtn(450, -50, 500, t-500, 0),250), "3");
        painter.drawText(QPoint(flyingBtn(900, 450, 500, t-500, 1),250), "2");
    }  else if (t >= 1000 && t < 1500) {
        painter.drawText(QPoint(flyingBtn(450, -50, 500, t-1000, 0),250), "2");
        painter.drawText(QPoint(flyingBtn(900, 450, 500, t-1000, 1),250), "1");
    } else if (t<=1750){
        painter.drawText(QPoint(350,250), "Go!");
    } else if (t == 1751) {
        timer4tick->start(300);

    } else {

        painter.setFont(font_small);
        painter.drawText(QPoint(750,40), "Score: "+ QString::number(score));
        snake->paintObj(wind);
        apple->paintObj(wind);
        wall->paintObj(wind);
        if (wind->game_over) {
            wind->toGameOver(score);
        }
    }

}

void GameWindow::mouseClick(QMouseEvent* event, GameWidget* m) {
    int x = event->pos().x();
    int y = event->pos().y();

    if(menu_btn.valid( x-20 , y-10 )) {
        m->setCurrent(new MainMenu());
    }
    event->accept();
}
void GameWindow::keyPressEvent(QKeyEvent * event, GameWidget* m) {

    if (event->key()==Qt::Key_Up) {
        dynamic_cast<Snake *>( snake )->setRoute(1);
    } else if (event->key()==Qt::Key_Down) {
        dynamic_cast<Snake *>( snake )->setRoute(3);
    } else if (event->key()==Qt::Key_Left) {
        dynamic_cast<Snake *>( snake )->setRoute(2);
    } else if (event->key()==Qt::Key_Right) {
        dynamic_cast<Snake *>( snake )->setRoute(0);
    }
    event->accept();

}

//_____________Rating Add_______________________________
RatingAdd::RatingAdd() {

}

void RatingAdd::paint(GameWidget * wind) {
    QPainter painter(wind);
    painter.setFont(font_normal);
    painter.setPen(pen);
    painter.drawImage(20, 20, menu_btn, 0, 0, 0, 0);
    painter.drawImage(800, 30, exit_btn, 0, 0, 0, 0);
    painter.drawImage(408, 500, enter_btn, 0, 0, 0, 0);
    QFontMetrics fm(font_normal);
    int nick_width = fm.width(wind->nickname);
    painter.drawText(QPoint(450-fm.width("Enter your name:")/2,250), "Enter your name:");
    painter.drawText(QPoint(450-nick_width/2,350), wind->nickname);

}

void RatingAdd::mouseClick(QMouseEvent* event, GameWidget* m) {
    int x = event->pos().x();
    int y = event->pos().y();

    if(menu_btn.valid( x-20 , y-20 )) {
        m->setCurrent(new MainMenu());
    }
    if(exit_btn.valid( x-800 , y-40 )) {
        exit(0);
    }
    if(enter_btn.valid( x-408 , y-500 )) {
        add_score(m);
        m->setCurrent(new Rating());
    }
    event->accept();
}

void RatingAdd::keyPressEvent(QKeyEvent* event, GameWidget* m) {

    if (event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return) {
        add_score(m);
        m->setCurrent(new Rating());
    } else {

        if ((event->key() >= Qt::Key_A && event->key() <= Qt::Key_Z) ||
                (event->key() >= Qt::Key_0 && event->key() <= Qt::Key_9)) {
            m->nickname += event->text();
            qDebug() << m->nickname;
        }
        if (event->key() == Qt::Key_Backspace) {
            m->nickname = m->nickname.left(m->nickname.length() - 1);
        }
    }
    event->accept();

}

void RatingAdd::add_score(GameWidget* m) {
    ofstream fout;
    fout.open("rating.dat", ios::app);
    fout << m->nickname.toStdString() << " " << m->score<<endl;
    fout.close();
}

//____________________________________________________________

void GameOver::paint(GameWidget *wind) {
    QPainter painter(wind);
    painter.setFont(font_big);
    painter.setPen(pen);
    painter.drawImage(20, 20, menu_btn, 0, 0, 0, 0);
    painter.drawImage(413, 500, exit_btn, 0, 0, 0, 0);
    painter.drawImage(398, 450, rating_btn, 0, 0, 0, 0);
    painter.drawText(QPoint(120,200), "Game over!");
    painter.setFont(font_normal);
    painter.drawText(QPoint(300,350), "Score: " + QString::number(wind->score));
    painter.setPen( QColor( 255, 255, 255, ( frameNumber < 128 )?
                                     frameNumber  : 255 - frameNumber ) );
    frameNumber %= 254;
    painter.setFont(QFont(family, 15));
    painter.drawText(QPoint(300,400), "Press enter to save the result");


}

void GameOver::mouseClick(QMouseEvent* event, GameWidget* m) {
    int x = event->pos().x();
    int y = event->pos().y();

    if(rating_btn.valid( x-398 , y-450 )) {
        m->setCurrent(new Rating());
    }
    if(exit_btn.valid( x-413 , y-500 )) {
        exit(0);
    }
    if(menu_btn.valid( x-20 , y-20 )) {
        m->setCurrent(new MainMenu());
    }
    event->accept();
}

void GameOver::keyPressEvent(QKeyEvent * event, GameWidget* m) {

    if (event->key()==Qt::Key_Enter || event->key()==Qt::Key_Return) {
        m->setCurrent(new RatingAdd());
    }
    event->accept();

}

