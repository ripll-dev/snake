#include "apple.h"
#include "QDebug"
#include "state.h"
Apple::Apple()
{
    srand(time(NULL));
    //apples.append(QRectF(5,5,10,10));
}



void Apple::paintObj(GameWidget* wind) {
    GameWindow* status = dynamic_cast<GameWindow*>(wind->current);
    QVector<QRect> blocks = status->getSnake();

    QPainter painter(wind);
    QPen pen{QColor("#ffffff")};
    painter.setBrush(QBrush(QColor("#ffffff")));
    painter.setPen(pen);

    if (apples.size() < 3) {
        apples.push_back(QRectF(5+30*(rand()%28+1),5+30*(rand()%17+2),20,20));
    }
    for (QRectF apple : apples) {

        if (blocks.count(QRect(apple.x()-2,apple.y()-2,24,24)) != 0) {
            status->apple_earned();
            apples.removeOne(apple);
            continue;
        }
        painter.drawEllipse(apple);
    }
}
