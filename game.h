#ifndef GAME_H
#define GAME_H
#include "gamewidget.h"

class Game
{
public:
    GameWidget *window;

    static Game& Instance()
    {
        static Game theSingleInstance;
        return theSingleInstance;
    }
    void run();

private:
    Game();
    ~Game();
    Game(const Game& root);
    Game& operator = (const Game&);

};

#endif // GAME_H
