#include "gamewidget.h"
#include "state.h"
#include <QTimer>

GameWidget::GameWidget(QWidget *parent) : QWidget(parent) {

    current = new LoadMenu;
    setEnabled(true);

    QTimer* time = new QTimer(this);
    connect (time, SIGNAL(timeout()), this, SLOT(nextAnimationFrame()));
    time->start(2);
    QPalette *palette = new QPalette;
    QColor *color = new QColor;
    color->setRgb(31,31,33);
    palette->setColor(backgroundRole(), *color);
    this->setPalette(*palette);
}

void GameWidget::paintEvent(QPaintEvent*) {
    current->paint(this);
}

void GameWidget::mouseReleaseEvent(QMouseEvent *event) {

    current->mouseClick(event, this);
}

void GameWidget::keyPressEvent(QKeyEvent * event) {

    current->keyPressEvent(event, this);
}

void GameWidget::nextAnimationFrame(){
    current->frameNumber++;
    update();
}


void GameWidget::toGameOver(int x) {
    score = x;
    game_over = false;
    setCurrent(new GameOver());
}


GameWidget::~GameWidget()
{

}
